FROM postgres:9.5
RUN echo "deb [trusted=yes] http://kong.bintray.com/kong-community-edition-deb stretch main" |  tee -a /etc/apt/sources.list && \
  apt update && apt install kong-community-edition curl -y

RUN sed '/bash/a sh -c "if [ -f /tmp/lock ] ; then exit 1; else touch /tmp/lock; fi ; chmod 777 /usr/local/kong -R; chmod 777 /usr/local/share/lua/5.1 -R ; until pg_isready -U postgres \> \/dev\/null 2\>\&1 \;do echo wait \; sleep 3\; done\; sleep 30 ; kong migrations bootstrap --vv ; sleep 10; \/usr\/local\/bin\/kong start --run-migrations --vv && sleep 10 && curl -X POST 127.0.0.1:8001/services -F \"name=admin\" -F \"url=http://127.0.0.1:8001\" && curl -X POST 127.0.0.1:8001/services/admin/routes -F paths[]=/admin" \&' -i /docker-entrypoint.sh
ENV POSTGRES_USER kong
ENV KONG_DATABASE postgres
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["postgres"]